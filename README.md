# React Native Native Library (POC)

This is a POC of a library made with React Native, configured to be built as a native module and be imported in a native platform project (only Android for now).

All the Javascript code is contained inside the bundle and can be added to any Android project without need to have a React Native environment (even Node).

This is a normal [React Native app](https://reactnative.dev/docs/environment-setup) converted to an Android lib as its described int these [docs]( https://developer.android.com/studio/projects/android-library?hl=es)

# Steps to create a new lib from scratch
1. Follow the steps to set up the RN env (if it is not set up yet) (https://reactnative.dev/docs/environment-setup)
2. Create a new RN app (node dependencies should be installed while app is initialized)
3. Open `android/app/build.gradle` and apply the next changes:

   1. Delete `applicationId` entry
   2. Open `android/app/build.gradle` and change
    ```
    plugins {
     id("com.android.application")
    }
    ```
    by
    ```
    plugins {
      id("com.android.library")
    }
    ```
    3. Search the next piece in the app/build.gradle file and remove it

    ```
    // applicationVariants are e.g. debug, release
      applicationVariants.all { variant ->
        variant.outputs.each { output ->
            // For each separate APK per architecture, set a unique version code as described here:
            // https://developer.android.com/studio/build/configure-apk-splits.html
            // Example: versionCode 1 will generate 1001 for armeabi-v7a, 1002 for x86, etc.
            def versionCodes = ["armeabi-v7a": 1, "x86": 2, "arm64-v8a": 3, "x86_64": 4]
            def abi = output.getFilter(OutputFile.ABI)
            if (abi != null) {  // null for the universal-debug, universal-release variants
                output.versionCodeOverride =
                        defaultConfig.versionCode * 1000 + versionCodes.get(abi)
            }

        }
    }
    ```
    4. And the next configuration
    ```
    configurations {
     customConfig.extendsFrom implementation
    }
    ```
    ```
    task copyDownloadableDepsToLibs(type: Copy) {
      from configurations.customConfig
      into 'libs'
    }
    ```
4. Run gradle `taskassembleRelease` to generate aar file
5. Run gradle task `copyDownloadableDepsToLibs` to generate the dependencies into the libs directive. 

At this point, the project should be ready to be compiled as an aar file. React Native developers can work in the RN repository, build and publish the module to a maven repository.

## Including the module into an existing project

The aar file generated in the previous section should be ready to go to be added into any Android project. The libs generated in step 5 are needed since React Native Android dependencies are not available in maven repositories, and are downloaded from npm when installing node dependencies. In order to keep a non dependent lib from node, these libs need to be exported from the RN project and added into the host project as dependencies.

The final step is to start the main Activity from the library. i.e

```
  Intent intent = new Intent(this, com.rnlibrary.MainActivity.class);
  startActivity(intent);
```
