import React, {useCallback, useEffect} from 'react';
import type {Node} from 'react';
import {
  Alert,
  Button,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  useEffect(() => {
    console.log('This message comes from Javascript side, hey!');
  }, []);

  const onPress = useCallback(() => {
    Alert.alert('Simple Button pressed from JS side');
  }, []);

  return (
    <View style={styles.container}>
      <Button title="Press me" onPress={onPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 10,
  },
});

export default App;
